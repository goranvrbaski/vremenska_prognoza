#!/usr/bin/env python
# -*- coding: utf-8 -*-

import codecs
import json
import os
import feedparser
import time

def validnost_prognoze(string):
    if string.find("Stanica:") != -1:
        return True
    else:
        return False

#proveriti return i formu
def prognoza(string):
    try:
        vreme = string.split()[string.split().index("Opis") + 2] + " " + \
                string.split()[string.split().index("Opis") + 3]
        if string[len(string)-1] == ";":
            vreme = vreme[:-1]
    except:
        vreme = string.split()[string.split().index("Opis") + 2]
        if string[len(string)-1] == ";":
            vreme = vreme[:-1]
    return{
        "Temperatura": string.split()[string.split().index("Temperatura:")+1],
        "Pritisak": string.split()[string.split().index("Pritisak:")+1],
        "PravacVetra": string.split()[string.split().index("Pravac")+2][:-1],
        "BrzinaVetra": string.split()[string.split().index("Brzina")+2],
        "Vlaznost": string.split()[string.split().index("m/s;")+2],
        "OpisVremena": vreme
    }

def godina_find(string):
    return string.split()[len(string)-1].split('.')[2]

def mesec_find(string):
    return string.split()[len(string)-1].split('.')[1]

def dan_find(string):
    return string.split()[len(string)-1].split('.')[0]

def sat_find(string):
    return string.split()[len(string)-2].split(':')[0]

def grad_find(string):
    try:
        grad = string.split()[1] + " " + string.split(2)
    except:
        grad = string.split()[1]
    return grad

def live_u_json(grad, godina, mesec, dan, sat, podaci):
    try:
        try:
            fajl = codecs.open("podaci/{0}/{1}/{2}.json".format(grad, godina, mesec) , \
                               encoding="utf-8")
            data = json.load(fajl)
        except:
            if not os.path.isdir("podaci/{0}".format(grad)):
                os.mkdir("podaci/{0}".format(grad))
            if not os.path.isdir("podaci/{0}/{1}".format(grad, godina)):
                os.mkdir("podaci/{0}/{1}".format(grad, godina))
            data = {}

        fajl = codecs.open("podaci/{0}/{1}/{2}.json".format(grad, godina, mesec), "w" , \
                           encoding="utf-8")

        try:
            data_dan = data[dan]
        except:
            data_dan = {}

        data_dan [sat] = podaci
        data [dan] = data_dan
        json.dump(data, fajl, indent=4, sort_keys=True)
        fajl.close()
        print ("Creating {0} - {1}/{2}/{3} : {4}".format(grad, godina, mesec, dan, sat))

    except IOError:
        print("greska pri upisu")

    except UnicodeError:
        print("unicode greska")

#zavrsiti zapis i parametre koji se salju u zapis
def write():
    rss = feedparser.parse('http://www.hidmet.gov.rs/latin/osmotreni/index.rss')

    print rss['feed']['title'] # PROVERITI GDE SE NALAZI VREME (DATUM)

    for i in range(0, len(rss['entries'])):
        if validnost_prognoze(rss['entries'][i].title):
            live_u_json(grad_find(rss['enteries'][i].title), godina_find(rss['feed']['title']), \
                        mesec_find(rss['feed']['title']), dan_find(rss['feed']['title']), \
                        sat_find(rss['feed']['title']), prognoza(rss['entries'][i].description))

#Zakomentarisana while petlja i vreme spavanja zbog uploada na GIT
if __name__ == '__main__':
    #while True:
        write()
        #time.sleep(3600)
