# Vremenska prognoza
Vremenska prognoza bazirana na podacima [Republičkog hidrometeorološkog zavoda Srbije](http://www.hidmet.gov.rs). Program je pisan u Pajton programskom jeziku. Primer možete pogleda na [Vreme Zrenjanin](https://twitter.com/vremezrenjanin) tviter nalogu.

Weather based on the data of the Hydrometeorological Service of Serbia. The program is written in the Python programming language. You can see example on [Vreme Zrenjanin](https://twitter.com/vremezrenjanin) twitter account.

## Sistemski zahtevi / System Requirements
- Python 2.7.*
- Python moduli (feedparser, codecs)
- Internet konekcija

## Instalacija / Installation
Pored standardne instalacije Pajton programskog jezika, treba instalirati modul feedparser, codecs kao i modul tweepy.

```sh
~$ sudo pip install feedparser
~$ sudo pip install codecs
~$ sudo pip install tweepy
```
### Autor i licenca / Author & License
- Goran Vrbaški - vrbaski.goran@gmail.com
- Gavrilo Bosakov - gavrilo.bos@gmail.com

LICENCA: [GNU GPL v3](LICENSE)

### Verzija / Version
0.1 beta - još uvek u razvoju / still developing

### Kontakt / Contact
- [mail](mailto:vrbaski.goran@lugozr.org?subject=Vremenska Prognoza)
- [twitter](https://twitter.com/goranvrbaski)

