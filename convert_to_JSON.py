#!/usr/bin/env python
# -*- coding: utf-8 -*-

import codecs
import csv
import json
import os

def validnost_tvita(string):
    if string.find(";") != -1:
        return True
    else:
        return False

#funkcije za pronalazenje potrebnih podataka iz CSV fajla preuzetog od Tweeter-a

def prognoza_find(string):
    if string.find("Opis vremena: ") != -1:
        try:
            vreme = string.split()[string.split().index("Opis") + 2] + " " + \
                    string.split()[string.split().index("Opis") + 3]
            if string[len(string)-1] == ";":
                vreme = vreme[:-1]
        except:
            vreme = string.split()[string.split().index("Opis") + 2]
            if string[len(string)-1] == ";":
                vreme = vreme[:-1]
    else:
        vreme = "No data"
    return {
        "Temperatura": string.split()[string.split().index("Temperatura:")+1],
        "Pritisak": string.split()[string.split().index("Pritisak:")+1],
        "PravacVetra": string.split()[string.split().index("Pravac")+2][:-1],
        "BrzinaVetra": string.split()[string.split().index("Brzina")+2],
        "Vlaznost": string.split()[string.split().index("m/s;")+2],
        "OpisVremena": vreme
    }

def godina_find(string):
    return string.split()[0].split("-")[0]

def mesec_find(string):
    return string.split()[0].split("-")[1]

def dan_find(string):
    return string.split()[0].split("-")[2]

def sat_find(string):
    return string.split()[1].split(":")[0]

def convert_u_json(godina, mesec, grad, podaci):
    try:
        if not os.path.isdir("podaci/"+grad):
            os.mkdir("podaci/"+grad)

        if not os.path.isdir("podaci/{0}/{1}".format(grad, godina)):
            os.mkdir("podaci/{0}/{1}".format(grad, godina))

        fajl = codecs.open("podaci/{0}/{1}/{2}.json".format(grad, godina, mesec), "w", \
                           encoding="utf-8")

        json.dump(podaci, fajl, indent=4, sort_keys=True)
        fajl.close()
        #print podaci
        print ("Writing data for {0} - {1}/{2}".format(grad,godina,mesec))

    except IOError:
        print("greska pri upisu")

    except UnicodeError:
        print("unicode greska")


def start():
    print("Converting csv to JSON files...")
    data_mesec = {}
    data_dan = {}
    mesec_separator = 0
    godina_separator = 0
    dan_separator = 0
    if not os.path.exists("podaci"):
            os.mkdir("podaci")
    with open('tweets.csv', 'r') as csvfile:

        csvfajl = csv.reader(csvfile, delimiter=',')
        for row in csvfajl:
            if validnost_tvita(row[5]):

                if mesec_separator != mesec_find(row[3]) or godina_separator != godina_find(row[3]):
                    if mesec_separator != 0:
                        data_mesec[dan_separator] = data_dan
                        convert_u_json(godina_separator, mesec_separator, "Zrenjanin", data_mesec)
                        #print data_mesec
                    data_mesec.clear()
                    mesec_separator = mesec_find(row[3])
                    godina_separator = godina_find(row[3])
                    dan_separator = dan_find(row[3])
                    data_dan.clear()

                if dan_separator != dan_find(row[3]) and dan_separator != 0:
                    data_mesec [dan_separator] = data_dan
                    data_dan.clear()
                    dan_separator = dan_find(row[3])

                data_dan[sat_find(row[3])] = prognoza_find(row[5])

    data_mesec[dan_separator] = data_dan
    convert_u_json(godina_separator, mesec_separator, "Zrenjanin", data_mesec)

    print("Finished!")

if __name__ == '__main__':
    start()
