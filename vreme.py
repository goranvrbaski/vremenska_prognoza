#!/usr/bin/env python
# -*- coding: utf-8 -*-

import codecs
import tweepy
import time

import feedparser
import tweepy


class TwitterWrapper:
    def __init__(self, consumer_key, consumer_secret, access_token, access_token_secret):
        auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
        auth.set_access_token(access_token, access_token_secret)
        self.api = tweepy.API(auth)

    def update_status(self, message):
        self.api.update_status(status=message)


def tvituj(text):
    print(text)
    # TODO: uncomment next two lines when want to test on real twitter application
    # tviter = TwitterWrapper(consumer_key, consumer_secret, access_token, access_token_secret)
    # tviter.update_status(poruka)

# parsiranje rss feed
rss = feedparser.parse('http://www.hidmet.gov.rs/latin/osmotreni/index.rss')

# beskonačna petlja
while True:
    # prolazak kroz sve "Stanice"
    for i in range(0, len(rss['entries'])):
        # uzimanje podataka samo iz "Stanice: Zrenjanin"
        if rss['entries'][i].title == "Stanica: Zrenjanin":
            podaci = rss['entries'][i].description.split(';')
            poruka = '#zrenjanin ' + podaci[1] + ';' + podaci[2] + ';' + podaci[3] + ';' + podaci[4] + ';' + podaci[5]\
                     + ';' + podaci[6]
            if len(poruka) > 140:
                poruka = '#zrenjanin ' + podaci[1] + ';' + podaci[2] + ';' + podaci[3] + ';' + podaci[4] + ';' \
                         + podaci[5]

    # tvituj(poruka)
    tvituj(poruka)
    try:
        # TODO: need to write in json file format
        fajl = codecs.open("test.txt", "a", "utf-8")
        fajl.write(poruka)
        fajl.close()
    except IOError:
        print("greska pri upisu")

    except UnicodeError:
        print("unicode greska")

    time.sleep(3600)
